require_relative '../lib/weather_client/_client.rb'
require_relative '../lib/weather_client/errors/global_weather_error.rb'
require_relative '../lib/weather_client/errors/soap_error.rb'
require_relative '../lib/weather_client.rb'

# require the helper module
require "savon/mock/spec_helper"
