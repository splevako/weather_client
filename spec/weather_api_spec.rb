require 'spec_helper'
 
describe GlobalWeather::Weather do
  before(:each) do
    @api = GlobalWeather::Weather.new :country_name => "Ukraine", :city_name => "Kyiv"   
  end
     
  it 'should parse cities by valid country' do    
    cities = @api.get_cities    
    cities.class.should be(Nori::StringWithAttributes)
    puts cities.inspect
  end
     
  it 'should parse weather for valid city' do
    weather = @api.get_weather        
    weather.class.should be(Nori::StringWithAttributes)
    puts weather.inspect
  end
  
  
end