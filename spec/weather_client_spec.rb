require 'spec_helper'
 
describe GlobalWeather::Client do
  before(:each) do
    @client = GlobalWeather.client   
  end

  it 'should raise an exception on invalid soap call' do   
   expect { @client.call(:get_weather, {}) }.to raise_error(GlobalWeather::Errors::SoapError)
  end   
  
  it 'should get cities by valid country' do    
    cities = @client.call(:get_cities_by_country, {:country_name => "Ukraine"})    
    cities.success?.should be(true)
    cities.soap_fault?.should be(false)
    cities.http_error?.should be(false) 
  end
  
  it "should get all the cities when country is empty" do
    cities = @client.call(:get_cities_by_country, {:country_name => ""})
    cities.success?.should be(true)
    cities.soap_fault?.should be(false)
    cities.http_error?.should be(false)     
  end 
  
  it "should return empty NewDataSet for non-existing country provided" do
     cities = @client.call(:get_cities_by_country, {:country_name => "dfsdfdfg"})
     cities.success?.should be(true)    
  end 
  
  it 'should get weather for valid city' do
    weather = @client.call(:get_weather, {:country_name => "Ukraine", :city_name => "Kyiv"})
    weather.success?.should be(true)
  end
  
  it "should return Data Not Found for non-existing city" do
    weather = @client.call(:get_weather, {:country_name => "Ukraine", :city_name => "Kyiv123"})
    weather.success?.should be(true)
  end
  
  it "should return current weather if data is empty" do
    weather = @client.call(:get_weather, {:country_name => "", :city_name => ""})
    weather.success?.should be(true)  
  end
  
end