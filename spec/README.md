Objective

To build a ruby wrapper for the Global Weather SOAP service (see reference) to be used in a rails project. The code should be production-ready (well tested, handling errors, etc), but not over-engineered.

What are we looking for?

- A well structured code

- Good OO design, with clear separation of concerns

- Well-defined public interfaces

- Well tested code (unit, functional and integration tests)

Important Notes

- Use the SOAP version of the service

- This wrapper should be built as a Ruby gem

- The gem should work if added in any ruby project with Bundler and a Gemfile

Advices

- Avoid re-inventing the wheel. Make use of good open source tools when building the gem.

Final Considerations

Take you time, the quality is more important than the speed. And let us know if you are not able to send the solution in the next days.