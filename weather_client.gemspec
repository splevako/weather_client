$:.push File.expand_path("../lib", __FILE__)
require "weather_client/version"

Gem::Specification.new do |s|
  s.name        = "weather_client"
  s.version     = GlobalWeather::VERSION
  s.date        = %q{2013-09-15}
  s.authors     = ["Sergey Plevako"]
  s.email       = ["splevako1@gmail.com"]
  s.homepage    = "https://bitbucket.org/splevako/weather_client"
  s.summary     = %q{Ruby wrapper for GlobalWeather}
  s.description = %q{Ruby wrapper for GlobalWeather web service}
  s.licenses    = ["MIT"]

  s.required_rubygems_version = '>= 1.3.6'
  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {spec}/*`.split("\n")
  s.require_paths = ["lib"]

  s.add_runtime_dependency 'savon', '~> 2.3.0'
  s.add_runtime_dependency 'i18n'
  s.add_runtime_dependency 'activesupport', '>= 3.0'
  
  s.add_development_dependency 'rspec'
  s.add_development_dependency 'rake', '~> 10.0'
  s.add_development_dependency 'rdoc', '>= 2.5.0'
  s.add_development_dependency 'vcr', '~> 2.3'
  s.add_development_dependency 'webmock', '~> 1.8.0'
end
