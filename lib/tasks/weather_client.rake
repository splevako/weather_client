require 'weather_client'

namespace :weather_client do

  desc "Configure WeatherClient."
  task :configure do
    unless WeatherClient.configured?
      WeatherClient.configure do |config|
        config.wsdl_url = ENV['WSDL_URL']
        config.timeout = ENV['TIMEOUT']        
      end  
      Savon.configure do |config|
        config.log = false
      end
      HTTPI.log = false
    end
  end

end
