require 'savon'
require 'i18n'

require 'active_support/core_ext'

require 'weather_client/version'
require 'weather_client/errors'
require 'weather_client/responses'
require 'weather_client/record'
require 'weather_client/weather'
require 'weather_client/cities_by_country'
require 'weather_client/configuration'
require 'weather_client/_client'

I18n.load_path << File.join(File.dirname(__FILE__), "config", "locales", "en.yml")

# WeatherClient is a web service to calculate and track sales tax for your ecommerce platform. Integration is easy to use.
# For information on configuring and using the WeatherClient API, look at the <tt>README</tt> file.
module GlobalWeather

  # WSDL location for WeatherClient API.
  WSDL_URL = "http://www.webservicex.net/globalweather.asmx?WSDL"

  # WeatherClient API version.
  API_VERSION = '0.1'

  class << self

    # WeatherClient gem configuration.
    attr_accessor :configuration

    # Returns true if the gem has been configured.
    def configured?
      !! self.configuration
    end

    # Configure the gem.
    def configure
      self.configuration ||= Configuration.new
      yield configuration
    end

    # Reset the current configuration.
    def reset!
      self.configuration = nil
    end

    # The configured SOAP client to the WeatherClient service.
    def client
      check_configuration!
      @@client ||= GlobalWeather::Client.new
    end

    private

      def check_configuration!       
        self.configuration.check! unless self.configuration.nil?
      end

  end

end
