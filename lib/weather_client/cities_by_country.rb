module GlobalWeather
  
class CitiesByCountry < Record
  
   class << self
     
    attr_accessor :country_name           

  def get_cities_by_country
    @cities_by_country ||= begin
      response = GlobalWeather.client.call :get_cities_by_country , { :country_name => country_name }
      cities_by_country = GlobalWeather::Responses::GetCitiesByCountry.parse response      
    end
  end
  
  
  def get_cities_by_country(country)
      response = @client.call(:get_cities_by_country) do
      message country_name: country       
    end
    
    if response.success?
      data = response.to_array(:get_cities_by_country_response, :get_cities_by_country_result).first
      if data
        data
      end
    end
    end
  end
  
  
end   
end