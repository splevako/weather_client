module GlobalWeather #:nodoc:
  module Responses #:nodoc:

    # A single item in the response to a WeatherClient GetWeather call.
    # 
    # See http://www.webservicex.net/globalweather.asmx?op=GetWeather
    class GetWeather < Base      
      
      response_type "get_weather_response/get_weather_result"
      #error_message "get_weather_response/get_weather_result"

      class << self
        # Parse a GlobalWeather response.
        #
        # === Parameters
        # [savon_response] SOAP response.
        #        
        def parse(savon_response)
          response = self.new(savon_response)
          weather = response.match("get_weather_response/get_weather_result")          
          #weather = response.match("get_weather_response/get_weather_result/current_weather")         
          #GlobalWeather::CurrentWeather.new({ 
          #    :location => weather[:location].strip,
          #    :time => weather[:time].strip,
          #    :wind => weather[:wind].strip,
          #    :visibility => weather[:visibility].strip,
          #    :sky_conditions => weather[:sky_conditions].strip,
          #    :temperature => weather[:temperature].strip,
          #    :dev_point => weather[:dev_point].strip,
          #    :status => weather[:status].strip
          #  })
          end
        end
      end      
         
  end
end
