module GlobalWeather #:nodoc:
  module Responses #:nodoc:

    # A single item in the response to a WeatherClient GetCitiesByCountry call.
    # 
    # See http://www.webservicex.net/globalweather.asmx?op=GetCitiesByCountry
    class GetCitiesByCountry < Base
              
      response_type "get_cities_by_country_response/get_cities_by_country_result"
      #error_message "get_cities_by_country_response/get_cities_by_country_result/new_data_set"

      class << self
        # Parse a GlobalWeather response.
        #
        # === Parameters
        # [savon_response] SOAP response.
        #       
        def parse(savon_response)
          response = self.new(savon_response)
          cities = response.match("get_cities_by_country_response/get_cities_by_country_result")
          #cities = response.match("get_cities_by_country_response/get_cities_by_country_result/new_data_set")
          #cities.map do |city|
          #  GlobalWeather::Table.new({ 
          #    :country => city[:country].strip,
          #    :city => city[:city].strip
          #  })
          #end
        end
      end
             
    end
  end
end
