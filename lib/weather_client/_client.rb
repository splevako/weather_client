require 'savon'

module GlobalWeather   
   
  attr_reader :client
  class Client < Savon::Client

    # Create a new client.
    def initialize 
      @client = Savon.client do    
        wsdl GlobalWeather::WSDL_URL        
        #endpoint "http://www.webservicex.net/globalweather.asmx"
        #namespace "http://www.webserviceX.NET"       
        open_timeout 300
        read_timeout 300
        convert_request_keys_to :camelcase
      end             
    end            

    # Make a safe SOAP call. 
    # Will raise a GlobalWeather::Errors::SoapError on error.
    #
    # === Parameters
    # [method] SOAP method.
    # [body] Body content.
    def call(method, body = {})
      safe do
        @client.call( method, :message => body,          
          
          :attributes => {   
             "xmlns:tns" => "http://www.webserviceX.NET",     
          }
        )
      end
    end       

    private     

      def safe &block
        begin
          yield
        rescue Savon::SOAPFault => e          
          raise GlobalWeather::Errors::SoapError.new(e)
        end
      end

  end
end
