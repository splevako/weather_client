require 'weather_client/errors/global_weather_error'
require 'weather_client/errors/soap_error'
require 'weather_client/errors/unexpected_soap_response_error'
require 'weather_client/errors/api_error'
