module GlobalWeather #:nodoc:  

    # A single item in the response to a WeatherClient Lookup API call.
    # 
    # See https://api.WeatherClient.net/1.0/WeatherClient.asmx?op=Lookup.
    class Table < Record

      # The Country attribute.
      attr_accessor :contry

      # The City attribute.
      attr_accessor :city
        
  end
end