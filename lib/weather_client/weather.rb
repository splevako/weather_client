module GlobalWeather
  class Weather < Record
    #class << self

      attr_accessor :city_name
      attr_accessor :country_name
      def get_weather
        @weather ||= begin
        response = GlobalWeather.client.call :get_weather , { :country_name => country_name, :city_name => city_name}
        weather = GlobalWeather::Responses::GetWeather.parse response
        end
      end

      def get_cities
        @cities ||= begin
        response = GlobalWeather.client.call :get_cities_by_country, {:country_name => country_name}
        weather = GlobalWeather::Responses::GetCitiesByCountry.parse response
        end
      end

   #end
  end
end