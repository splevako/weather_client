module GlobalWeather #:nodoc:  

    # A single item in the response to a WeatherClient Lookup API call.
    # 
    # See https://api.WeatherClient.net/1.0/WeatherClient.asmx?op=Lookup.
    class CurrentWeather < Record

    attr_accessor :location
    attr_accessor :time
    attr_accessor :wind
    attr_accessor :visibility
    attr_accessor :sky_conditions
    attr_accessor :temperature
    attr_accessor :dev_point
    attr_accessor :relative_humidity
    attr_accessor :pressure
    attr_accessor :status
        
  end
end